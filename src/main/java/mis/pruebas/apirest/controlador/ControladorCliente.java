package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.servicios.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping(Rutas.CLIENTES)
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;

    @GetMapping
    public List<Cliente> obtenerClientes(@RequestParam int pagina, @RequestParam int cantidad) {
        try {
            return this.servicioCliente.obtenerClientes(pagina - 1, cantidad);} catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);}
    }

    // POST http://localhost:9000/api/v1/clientes + DATOS -> agregarCliente(Cliente)
    // Probando documento con standar y longitud especifico:
    // EJEMPLO: 8883 7774 6664 1234
    // EJEMPLO2: 9993-8884-1254-1235
    @PostMapping
    public void agregarCliente(@RequestBody Cliente cliente)
    {
        Pattern pat = Pattern.compile("(\\d){4}[- ](\\d){4}[- ](\\d){4}[- ](\\d){4}");
        Matcher mat = pat.matcher(cliente.documento);
        if (!mat.find()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Formato invalido de tarjeta");
        }
        if (cliente.documento != null && cliente.documento.length() > 19 ) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Longitud invalida del documento");
        }
        this.servicioCliente.insertarClienteNuevo(cliente);}

    @GetMapping("/{documento}")
    public Cliente obtenerUnCliente(@PathVariable String documento) {
        try {
            return this.servicioCliente.obtenerCliente(documento);}
        catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
    @PutMapping("/{documento}")
    public void reemplazarUnCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cliente cliente) {
        try {
            cliente.documento = nroDocumento;
            this.servicioCliente.guardarCliente(cliente);
        } catch(Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"No existe el Cliente");
        }
    }
    @PatchMapping("/{documento}")
    public void emparacharUnCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cliente cliente) {
        cliente.documento = nroDocumento;
        this.servicioCliente.emparcharCliente(cliente);
    }
    @DeleteMapping("/{documento}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarUnCliente(@PathVariable String documento) {
        try {
            this.servicioCliente.borrarCliente(documento);
        } catch(Exception x) {}
    }
}